﻿// fyazig.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <regex.h>
#include <string>


bool reg_matches(const char *str, const char *pattern)
{															//Begin reg_matches

															/*ОПИСАНИЕ ПЕРЕМЕННЫХ*/

	regex_t re;												//переменная структуры для обработки regex;
	int ret;												//переменная для хранения возвращаемого значения функции;

															/*ОПРЕДЕЛЕНИЕ ПЕРЕМЕННЫХ (INITIALIZATION)*/

	ret = 0;												//установка начального значения переменной;

	if (regcomp(&re, pattern, REG_EXTENDED) != 0)			//Если функция regcomp не смогла преобразовать строку в нужный для анализа вид;
	{
		regfree(&re);										//очищаем структуру обработки regex;
		return false;										//возвращаем false;
	}

	if (regexec(&re, str, (size_t)0, NULL, 0) == 0)			//Если функция regexec обнаружила соответствие шаблону (паттерну);								
	{
		regfree(&re);										//очищаем структуру обработки regex;
		return true;										//возвращаем true;
	}
	else
	{													//соответствий не найдено
		regfree(&re);										//очищаем структуру обработки regex;
		return false;										//если строка не соответствует шаблону (паттерну);
	}
}

int main()
{
	static const char *Pattern = "^(a+b+c+)$";
	std::string inputString;
	std::cout << "Enter a line: ";
	std::getline(std::cin, inputString);
	if (!reg_matches(inputString.c_str(), Pattern))									//Если входная строка не соответствует паттерну Hostname,
	{																		//(т.е. необрабатываемый нами мусор);
		std::cout << "The entered line does NOT correspond to a pattern!\n";  //Выводим Инфо на экран;
		return 1;
	}

	std::cout << "The entered line corresponds to a pattern!\n";
	return 0;
}